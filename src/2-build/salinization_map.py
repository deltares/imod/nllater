from pathlib import Path
import xarray as xr
import numpy as np
import imod
from scipy.ndimage import percentile_filter, uniform_filter, label
from rasterio.crs import CRS

crs = CRS.from_epsg(28992)  # Amersfoort RD New
interim = Path("data/2-interim")

# open intermediate results
has_shallow = imod.rasterio.open(interim / "has_shallow2.tif")
#has_medium = imod.rasterio.open(interim / "has_medium.tif")
has_flushing = imod.rasterio.open(interim / "has_flushing.tif")
has_propagation = imod.rasterio.open(interim / "has_propagation.tif")
has_sl_now = imod.rasterio.open(interim / "has_sl_now.tif")
has_sl_future = imod.rasterio.open(interim / "has_sl_future.tif")
has_sh_future = imod.rasterio.open(interim / "has_shallow_future.tif")
no_dunes = imod.rasterio.open(interim / "no_dunes.tif")
#has_sl_incr_slr = imod.rasterio.open(interim / "has_shallow.tif")

def write_tif(fn, da):
    da.attrs["crs"] = crs
    imod.rasterio.write(fn, da.squeeze(drop=True), dtype=float)

# categorize: current issues
# 1. shallow saline groundwater
# 2. surface water salinization requires flushing
# 3. both
current = has_shallow + 2 * (has_flushing)#.where(has_sl_now == 1, 0))
#current = current.where(current < 3, 1)
current = current.where((no_dunes==1) | current.isnull(), 0)
write_tif(interim / "current.tif", current)

# categorize: future issues
# 1. shallow saline groundwater
# 2. surface water salinization requires flushing
# 3. both
future = has_sh_future + 2 * ((has_flushing == 1) | (has_sl_future == 1))
#future = future.where(future < 3, 1)
future = future.where((no_dunes==1) | future.isnull(), 0)
write_tif(interim / "future.tif", future)


def focal_median(da,size=(5,4)):
    # filter outcome, apply a focal median filter
    filtered = percentile_filter((da.fillna(1e20)).values, percentile=50, size=size)
    da = da.copy(data=filtered)
    return da.where(da < 1e20)

def isolated_cells(da, cutoff=1e2):
    """ Function that return areas that are 
    smaller than the cutoff (in number of cells)
    """
    active_cells = da.notnull()#.fillna(0).astype(int)
    labels, nf = label(active_cells, structure=np.ones((3,3)))
    unique, counts = np.unique(labels, return_counts=True)
    isolated = xr.full_like(da, False, dtype=bool)
    if (counts < cutoff).any():  # less than cutoff: unconnected region
        isolated = unique[counts < cutoff]
        isolated = np.isin(labels, isolated)
        isolated = da.copy(data=isolated)  # set bnd to zero for isolated regions
    return isolated

def remove_isolated(da, cutoff=1e2):
    values = da.to_series().dropna().unique()
    isolated = da.isnull()
    for v in values:
        isolated = isolated | isolated_cells(da.where(da==v), cutoff=cutoff)
    return da.where(~isolated)


# filter and remove isolated cells
current_f = focal_median(current, size=(5,4))
tmp = remove_isolated(current_f, 50)
current_f = imod.prepare.fill(tmp).where(current_f.notnull())
write_tif(interim / "current_f.tif", current_f)
#isolated = isolated_cells(current_f.where(current_f==1), cutoff=10) | isolated_cells(current_f.where(current_f==2), cutoff=10) | isolated_cells(current_f.where(current_f==3), cutoff=10)
#write_tif(interim / "test.tif", test)

future_f = focal_median(future, size=(5,4))
tmp = remove_isolated(future_f, 50)
future_f = imod.prepare.fill(tmp).where(future_f.notnull())
write_tif(interim / "future_f.tif", future_f)


# create shapefile
current_poly = imod.prepare.polygonize(current_f)
current_poly.to_file(interim / "current.shp")