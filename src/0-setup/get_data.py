from pathlib import Path
from shutil import copy2

extdir = Path("data/1-external")
(extdir / "depthsalt").mkdir(exist_ok=True)
(extdir / "coverlayer").mkdir(exist_ok=True)
(extdir / "saltload").mkdir(exist_ok=True)
(extdir / "flushing").mkdir(exist_ok=True)

fn_depthsalt = r"c:\Svnrepos\nhi-fresh-salt\data\5-visualization\dataportal\3dchloride_depthfreshbrack_mMSL_filtered.tif"
fn_flushing = r"c:\Users\delsman\OneDrive - Stichting Deltares\Documents\2018\Deltascenarios\Doorspoeling\gis\lsws_doorspoeling.shp"

