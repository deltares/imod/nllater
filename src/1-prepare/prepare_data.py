from pathlib import Path
import geopandas as gpd
import imod
import xarray as xr
from rasterio.crs import CRS

crs = CRS.from_epsg(28992)  # Amersfoort RD New
interim = Path("data/2-interim")

fn_top = r"c:\Svnrepos\nhi-fresh-salt\data\1-external\LHM41_lagenmodel\top\MDL_TOP_l1.idf"
fn_depthsalt = r"c:\Svnrepos\nhi-fresh-salt\data\5-visualization\dataportal\3dchloride_depthfreshbrack_mMSL_filtered.tif"
fn_flushing = r"c:\Users\delsman\OneDrive - Stichting Deltares\Documents\2018\Deltascenarios\Doorspoeling\gis\lsws_doorspoeling.shp"
fn_head_aut = r"c:\Svnrepos\nhi-fresh-salt\data\4-output\v2.3.0\head\head_21000101_l11.idf"
fn_head_slr = r"c:\Svnrepos\nhi-fresh-salt\data\4-output\v2.3.0_scenario2\head\head_21000101_l11.idf"
fn_sl_aut = r"c:\Svnrepos\nhi-fresh-salt\data\5-visualization\v2.3.0\saltload\saltload_*.idf"
fn_sl_slr = r"c:\Svnrepos\nhi-fresh-salt\data\5-visualization\v2.3.0_scenario2\saltload\saltload_*.idf"
fn_conc_aut = r"c:\Svnrepos\nhi-fresh-salt\data\4-output\v2.3.0\conc\conc*.idf"
fn_conc_slr = r"c:\Svnrepos\nhi-fresh-salt\data\4-output\v2.3.0_scenario2\conc\conc*.idf"
fn_layermodel = r"c:\Svnrepos\nhi-fresh-salt\data\2-interim\layermodel_corr.nc"
fn_landuse = r"c:\GISDATA\lgn6\geogegevens\raster\lgn6\lgn6"

def write_tif(fn, da):
    da.attrs["crs"] = crs
    imod.rasterio.write(fn, da.squeeze(drop=True), dtype=float)

# shallow saline
top = imod.idf.open(fn_top, pattern="{name}")
depthsalt = imod.rasterio.open(fn_depthsalt, pattern="{name}")
has_shallow = (top - depthsalt) < 5. # within 5 m
write_tif(interim / "has_shallow.tif", has_shallow)
has_medium = (top - depthsalt) < 25. # within 25 m
write_tif(interim / "has_medium.tif", has_medium)

# flushing
flushing = gpd.read_file(fn_flushing)
flushing = imod.prepare.rasterize(flushing, like=depthsalt, column="LIT_mm")
write_tif(interim / "lswflushing.tif", flushing)
has_flushing = flushing > 0
write_tif(interim / "has_flushing.tif", has_flushing)

# head propagation aq1 SLR
head_aut = imod.idf.open(fn_head_aut, pattern="{name}")
head_slr = imod.idf.open(fn_head_slr, pattern="{name}")
propagation = head_slr - head_aut
write_tif(interim / "propagation.tif", propagation)
has_propagation = propagation > 0.05 # over 5%
write_tif(interim / "has_propagation.tif", has_propagation)

# salt load
sl_cutoff = 25
sl_aut = imod.idf.open(fn_sl_aut)
sl_slr = imod.idf.open(fn_sl_slr)
has_sl_now = sl_aut.sel(time="2020-01-01") > sl_cutoff
write_tif(interim / "has_sl_now.tif", has_sl_now)

# sl increase autonomous
sl_incr_aut = (sl_aut.sel(time="2100-01-01") - sl_aut.sel(time="2020-01-01"))
sl_incr_aut = (sl_incr_aut / sl_aut.sel(time="2020-01-01")).where(sl_aut.sel(time="2100-01-01") > sl_cutoff)
has_sl_incr_aut = sl_incr_aut > 0.1 # over 10% increase
write_tif(interim / "has_sl_incr_aut.tif", has_sl_incr_aut)

# sl increase SLR
sl_incr_slr = (sl_slr.sel(time="2100-01-01") - sl_aut.sel(time="2100-01-01"))
sl_incr_slr = (sl_incr_slr / sl_aut.sel(time="2100-01-01")).where(sl_slr.sel(time="2100-01-01") > sl_cutoff)
has_sl_incr_slr = sl_incr_slr > 0.1 # over 10% increase
write_tif(interim / "has_sl_incr_slr.tif", has_sl_incr_slr)

# future problem, not now?
has_sl_future = (sl_aut.sel(time="2020-01-01") < sl_cutoff) & (sl_slr.sel(time="2100-01-01") > sl_cutoff)
write_tif(interim / "has_sl_future.tif", has_sl_future)

# depth saline water 2
layermodel = xr.open_dataset(fn_layermodel)
z = 0.5 * layermodel["top"] + 0.5 * layermodel["bot"]
z = z.assign_coords(dz=layermodel.dz)
conc_aut = imod.idf.open(fn_conc_aut)
conc_slr = imod.idf.open(fn_conc_slr)
exc_now, _ = imod.evaluate.interpolate_value_boundaries(conc_aut.sel(time="2020-01-01"), z, 1.)
exc_aut, _ = imod.evaluate.interpolate_value_boundaries(conc_aut.sel(time="2100-01-01"), z, 1.)
exc_slr, _ = imod.evaluate.interpolate_value_boundaries(conc_slr.sel(time="2100-01-01"), z, 1.)
has_shallow2 = (top - exc_now.sel(boundary=0)) < 2.
write_tif(interim / "has_shallow2.tif", has_shallow2)
has_shallow_aut = (top - exc_aut.sel(boundary=0)) < 2.
write_tif(interim / "has_shallow_aut.tif", has_shallow_aut)
has_shallow_slr = (top - exc_slr.sel(boundary=0)) < 2.
write_tif(interim / "has_shallow_slr.tif", has_shallow_slr)
has_shallow_future = (exc_slr.sel(boundary=0) < exc_now.sel(boundary=0)) & has_shallow_slr
write_tif(interim / "has_shallow_future.tif", has_shallow_future)

# landuse
landuse = imod.rasterio.open(fn_landuse)
landuse250 = imod.prepare.reproject(landuse, like=top, src_nodata=255.).compute()
landuse250 = landuse250.assign_coords(dx=250., dy=-250.)
landuse250 = landuse250.where(landuse250 != landuse250.nodata)
no_dunes = landuse250.where((landuse250 < 30) | (landuse250 > 35))
no_dunes = no_dunes.notnull()
write_tif(interim / "landuse.tif", landuse250)
write_tif(interim / "no_dunes.tif", no_dunes)